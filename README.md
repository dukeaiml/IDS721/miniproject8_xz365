# mini-project8: Rust Command-Line Tool with Testing
> Xingyu, Zhang (NetID: xz365)

## Build And Test A Rust Command-Line Tool
### Create New Rust Project
Run the following commands
```bash
cargo new project_name
cd project_name
```
to create a new rust project and switch to the project's root directory.

### Add Dependencies To [Cargo.toml](./rust_cmd_tool/Cargo.toml)

```toml
[dependencies]
clap = "2.33.3"
csv = "1.3.0"
tempfile = "3.10.1"
```

__`clap`__: A simple to use, efficient, and full-featured Command Line Argument Parser. This lib is implemented to perform command-line argument parsing.
__`csv`__: Fast CSV parsing with support for serde. This lib is used to load and parse the input data.
__`tempfile`__: A library for managing temporary files and directories. This lib is used for creating temporary files for testing.

### Write Functions To Perform Data Processing in [main.rs](./miniproject7/src/main.rs)
This command-line tool allows users to calculate the average grade of all students collected in a `csv file`. 
1. It first uses the `clap` crate for parsing input command-line arguments. 
```rust
fn main() {
    let matches = App::new("Grade Analyzer")
        .version("1.0")
        .author("Xingyu")
        .about("Analyzes student grades")
        .arg(
            Arg::with_name("file")
                .short("f")
                .long("file")
                .value_name("FILE")
                .help("Sets the path to the CSV file containing students' grades")
                .takes_value(true),
        )
        .get_matches();

    let file_path = matches.value_of("file").unwrap_or("data.csv");

    let grades = match read_data_from_csv(file_path) {
        Ok(data) => data,
        Err(e) => {
            eprintln!("Error reading data: {}", e);
            return;
        }
    };

    for student in &grades {
        println!("Name: {}, Grade: {}", student.name, student.grade);
    }

    let average_grade = calculate_average_grade(&grades);
    println!("Average Grade: {:.2}", average_grade);
}
```

2. Then parse the data according to the structure defined
```rust
pub struct Student {
    pub name: String,
    pub grade: u32,
}

pub fn read_data_from_csv(file_path: &str) -> Result<Vec<Student>, Box<dyn Error>> {
    let mut students = Vec::new();
    let file = File::open(file_path)?;
    let mut rdr = ReaderBuilder::new().from_reader(file);

    for result in rdr.records() {
        let record = result?;
        let name = record.get(0).ok_or("Invalid record")?.to_string();
        let grade = record
            .get(1)
            .ok_or("Invalid record")?
            .parse::<u32>()?;
        students.push(Student { name, grade });
    }

    Ok(students)
}
```

3. After that, calculate the average number of all input data
```rust
pub fn calculate_average_grade(students: &[Student]) -> f64 {
    let total_grades: u32 = students.iter().map(|student| student.grade).sum();
    total_grades as f64 / students.len() as f64
}
```

4. Finally, print out all data and as well as the average grade in `main()` function.

### Build And Run
First, build the project using following command:
```bash
cargo build
```

Then, run the function and check the output.(Replace ```data.csv``` with the path to the CSV file containing student grade data)
```bash
cargo run -- -f data.csv
```
__Output__

![output](./assets/function.png)

### Test
There are four test cases included in this project:
- __`test_read_valid_csv_data()`__: This test ensures that the `read_data_from_csv()` function correctly reads valid CSV data from a file and returns a vector of `Student` structs. It checks that the number of records read matches the number of records expected and that each record is parsed accurately.
  
- __`test_read_invalid_csv_data()`__: This test checks that the read_data_from_csv()` function handles invalid CSV data gracefully. It creates a temporary file with invalid data, such as a missing grade for a student, and verifies that attempting to read data from it results in an error.

- __`test_read_non_existent_file()`__: This test verifies that the `read_data_from_csv()` function handles non-existent file paths gracefully by returning an error when attempting to read data from a file that does not exist.

- __`test_calculate_average_grade()`__: This test ensures that the `calculate_average_grade()` function accurately calculates the average grade from a vector of `Student` structs. It provides a vector of students with known grades and verifies that the calculated average matches the expected value.

```rust
#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Write;

    #[test]
    fn test_read_valid_csv_data() {
        // Test valid CSV data
        let csv_data = "Alice,90\nBob,85\nCharlie,95\nIrene,100";
        let mut temp_file = tempfile::NamedTempFile::new().unwrap();
        write!(temp_file, "{}", csv_data).unwrap();
        let file_path = temp_file.path().to_str().unwrap();
        let result = read_data_from_csv(file_path);
        assert!(result.is_ok());
        let data = result.unwrap();
        assert_eq!(data.len(), 3);
    }

    #[test]
    fn test_read_invalid_csv_data() {
        // Test invalid CSV data
        let invalid_csv_data = "Alice,90\nBob\nCharlie,95"; // Missing grade for "Bob"
        let mut temp_file_invalid = tempfile::NamedTempFile::new().unwrap();
        write!(temp_file_invalid, "{}", invalid_csv_data).unwrap();
        let invalid_file_path = temp_file_invalid.path().to_str().unwrap();
        let result_invalid = read_data_from_csv(invalid_file_path);
        assert!(result_invalid.is_err());
    }

    #[test]
    fn test_read_non_existent_file() {
        // Test non-existent file
        let non_existent_file_path = "/path/to/non_existent_file.csv";
        let result_non_existent = read_data_from_csv(non_existent_file_path);
        assert!(result_non_existent.is_err());
    }

    #[test]
    fn test_calculate_average_grade() {
        // Test calculating average grade
        let students = vec![
            Student { name: "Alice".to_string(), grade: 90 },
            Student { name: "Bob".to_string(), grade: 85 },
            Student { name: "Charlie".to_string(), grade: 95 },
        ];
        let average_grade = calculate_average_grade(&students);
        assert_eq!(average_grade, 90.0);
    }
}
```
For testing, run:
```bash
cargo test
```

#### Test Results
![test results](./assets/test.png) 