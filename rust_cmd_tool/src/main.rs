use clap::{App, Arg};
use csv::ReaderBuilder;
use std::error::Error;
use std::fs::File;

pub struct Student {
    pub name: String,
    pub grade: u32,
}

pub fn read_data_from_csv(file_path: &str) -> Result<Vec<Student>, Box<dyn Error>> {
    let mut students = Vec::new();
    let file = File::open(file_path)?;
    let mut rdr = ReaderBuilder::new().from_reader(file);

    for result in rdr.records() {
        let record = result?;
        let name = record.get(0).ok_or("Invalid record")?.to_string();
        let grade = record
            .get(1)
            .ok_or("Invalid record")?
            .parse::<u32>()?;
        students.push(Student { name, grade });
    }

    Ok(students)
}

pub fn calculate_average_grade(students: &[Student]) -> f64 {
    let total_grades: u32 = students.iter().map(|student| student.grade).sum();
    total_grades as f64 / students.len() as f64
}

fn main() {
    let matches = App::new("Grade Analyzer")
        .version("1.0")
        .author("Xingyu")
        .about("Analyzes student grades")
        .arg(
            Arg::with_name("file")
                .short("f")
                .long("file")
                .value_name("FILE")
                .help("Sets the path to the CSV file containing students' grades")
                .takes_value(true),
        )
        .get_matches();

    let file_path = matches.value_of("file").unwrap_or("data.csv");

    let grades = match read_data_from_csv(file_path) {
        Ok(data) => data,
        Err(e) => {
            eprintln!("Error reading data: {}", e);
            return;
        }
    };

    for student in &grades {
        println!("Name: {}, Grade: {}", student.name, student.grade);
    }

    let average_grade = calculate_average_grade(&grades);
    println!("Average Grade: {:.2}", average_grade);
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Write;

    #[test]
    fn test_read_valid_csv_data() {
        // Test valid CSV data
        let csv_data = "Alice,90\nBob,85\nCharlie,95\nIrene,100";
        let mut temp_file = tempfile::NamedTempFile::new().unwrap();
        write!(temp_file, "{}", csv_data).unwrap();
        let file_path = temp_file.path().to_str().unwrap();
        let result = read_data_from_csv(file_path);
        assert!(result.is_ok());
        let data = result.unwrap();
        assert_eq!(data.len(), 3);
    }

    #[test]
    fn test_read_invalid_csv_data() {
        // Test invalid CSV data
        let invalid_csv_data = "Alice,90\nBob\nCharlie,95"; // Missing grade for "Bob"
        let mut temp_file_invalid = tempfile::NamedTempFile::new().unwrap();
        write!(temp_file_invalid, "{}", invalid_csv_data).unwrap();
        let invalid_file_path = temp_file_invalid.path().to_str().unwrap();
        let result_invalid = read_data_from_csv(invalid_file_path);
        assert!(result_invalid.is_err());
    }

    #[test]
    fn test_read_non_existent_file() {
        // Test non-existent file
        let non_existent_file_path = "/path/to/non_existent_file.csv";
        let result_non_existent = read_data_from_csv(non_existent_file_path);
        assert!(result_non_existent.is_err());
    }

    #[test]
    fn test_calculate_average_grade() {
        // Test calculating average grade
        let students = vec![
            Student { name: "Alice".to_string(), grade: 90 },
            Student { name: "Bob".to_string(), grade: 85 },
            Student { name: "Charlie".to_string(), grade: 95 },
        ];
        let average_grade = calculate_average_grade(&students);
        assert_eq!(average_grade, 90.0);
    }
}